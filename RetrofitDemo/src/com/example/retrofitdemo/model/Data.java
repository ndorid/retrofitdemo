
package com.example.retrofitdemo.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Data {

    @Expose
    private String id;
    @Expose
    private String email;
    @SerializedName("user_roles_id")
    @Expose
    private String userRolesId;
    @SerializedName("last_login_timestamp")
    @Expose
    private Integer lastLoginTimestamp;
    @Expose
    private String status;
    @Expose
    private String step;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("twitter_url")
    @Expose
    private Object twitterUrl;
    @SerializedName("li_url")
    @Expose
    private String liUrl;
    @Expose
    private Object url;
    @Expose
    private List<Object> UserLinkedin = new ArrayList<Object>();
    @SerializedName("profile_image_url")
    @Expose
    private String profileImageUrl;
    @SerializedName("profile_banner_url")
    @Expose
    private String profileBannerUrl;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The userRolesId
     */
    public String getUserRolesId() {
        return userRolesId;
    }

    /**
     * 
     * @param userRolesId
     *     The user_roles_id
     */
    public void setUserRolesId(String userRolesId) {
        this.userRolesId = userRolesId;
    }

    /**
     * 
     * @return
     *     The lastLoginTimestamp
     */
    public Integer getLastLoginTimestamp() {
        return lastLoginTimestamp;
    }

    /**
     * 
     * @param lastLoginTimestamp
     *     The last_login_timestamp
     */
    public void setLastLoginTimestamp(Integer lastLoginTimestamp) {
        this.lastLoginTimestamp = lastLoginTimestamp;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The step
     */
    public String getStep() {
        return step;
    }

    /**
     * 
     * @param step
     *     The step
     */
    public void setStep(String step) {
        this.step = step;
    }

    /**
     * 
     * @return
     *     The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * 
     * @param firstName
     *     The first_name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * 
     * @return
     *     The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * 
     * @param lastName
     *     The last_name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * 
     * @return
     *     The twitterUrl
     */
    public Object getTwitterUrl() {
        return twitterUrl;
    }

    /**
     * 
     * @param twitterUrl
     *     The twitter_url
     */
    public void setTwitterUrl(Object twitterUrl) {
        this.twitterUrl = twitterUrl;
    }

    /**
     * 
     * @return
     *     The liUrl
     */
    public String getLiUrl() {
        return liUrl;
    }

    /**
     * 
     * @param liUrl
     *     The li_url
     */
    public void setLiUrl(String liUrl) {
        this.liUrl = liUrl;
    }

    /**
     * 
     * @return
     *     The url
     */
    public Object getUrl() {
        return url;
    }

    /**
     * 
     * @param url
     *     The url
     */
    public void setUrl(Object url) {
        this.url = url;
    }

    /**
     * 
     * @return
     *     The UserLinkedin
     */
    public List<Object> getUserLinkedin() {
        return UserLinkedin;
    }

    /**
     * 
     * @param UserLinkedin
     *     The UserLinkedin
     */
    public void setUserLinkedin(List<Object> UserLinkedin) {
        this.UserLinkedin = UserLinkedin;
    }

    /**
     * 
     * @return
     *     The profileImageUrl
     */
    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    /**
     * 
     * @param profileImageUrl
     *     The profile_image_url
     */
    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    /**
     * 
     * @return
     *     The profileBannerUrl
     */
    public String getProfileBannerUrl() {
        return profileBannerUrl;
    }

    /**
     * 
     * @param profileBannerUrl
     *     The profile_banner_url
     */
    public void setProfileBannerUrl(String profileBannerUrl) {
        this.profileBannerUrl = profileBannerUrl;
    }

}
