
package com.example.retrofitdemo.model;
import com.google.gson.annotations.Expose;



public class ApiResponse {

    @Expose
    private String status;
    @Expose
    private Token token;
    @Expose
    private Data data;

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The token
     */
    public Token getToken() {
        return token;
    }

    /**
     * 
     * @param token
     *     The token
     */
    public void setToken(Token token) {
        this.token = token;
    }

    /**
     * 
     * @return
     *     The data
     */
    public Data getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setData(Data data) {
        this.data = data;
    }

}
