
package com.example.retrofitdemo.model;

import com.google.gson.annotations.Expose;


public class Token {

    @Expose
    private String value;
    @Expose
    private Integer validity;

    /**
     * 
     * @return
     *     The value
     */
    public String getValue() {
        return value;
    }

    /**
     * 
     * @param value
     *     The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 
     * @return
     *     The validity
     */
    public Integer getValidity() {
        return validity;
    }

    /**
     * 
     * @param validity
     *     The validity
     */
    public void setValidity(Integer validity) {
        this.validity = validity;
    }

}
