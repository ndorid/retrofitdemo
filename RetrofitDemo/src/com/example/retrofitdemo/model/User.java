
package com.example.retrofitdemo.model;

import com.google.gson.annotations.Expose;


public class User {

    @Expose
    private ApiResponse apiResponse;

    /**
     * 
     * @return
     *     The apiResponse
     */
    public ApiResponse getApiResponse() {
        return apiResponse;
    }

    /**
     * 
     * @param apiResponse
     *     The apiResponse
     */
    public void setApiResponse(ApiResponse apiResponse) {
        this.apiResponse = apiResponse;
    }

}
