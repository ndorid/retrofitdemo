package com.example.retrofitdemo;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

import com.example.retrofitdemo.model.User;


public interface RetrofitInterface {

	 public static final String BASE_URL = "https://qa.trelliscience.com";
	 public static final String BASE_URL1 = "http://www.csitebooks.org";
   
	 	@FormUrlEncoded
	 	@POST("/api/users/userLogin.json")
	 	public void  userLogin(@Field("email") String email,
	              @Field("password") String pass,
	              Callback<User> response);
	 	
	 	@FormUrlEncoded
	 	@POST("/api/libraries/getDocuments.json")
	 	public void getDocuments(@Field("group_id") String groupid, 
	 			Callback<User> response);
	 	
	 	@GET("/api/get_recent_posts/")
	 	public void test(
	 			Callback<User> response);
}
