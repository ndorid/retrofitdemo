package com.example.retrofitdemo;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class VolleyPostActivity extends Activity{
	
//	private TextView tv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
//			tv = (TextView)findViewById(R.id.tv1);
		
		final ProgressDialog pDialog = new ProgressDialog(VolleyPostActivity.this);
	      pDialog.setMessage("Loading...");
	      pDialog.show();
	      
	      RequestQueue mRequestQueue = Volley.newRequestQueue(this);
	      String url = "http://192.168.56.1/nikhil/";
			StringRequest mStringRequest = new StringRequest(Request.Method.POST,
					url, new Response.Listener<String>() {
						@Override
						public void onResponse(String response) {
							// response
							Log.d("Response", response);
							pDialog.hide();
						}
					}, new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							// error
							Log.d("Error.Response", error.getMessage());
							pDialog.hide();
						}
					}) {
				@Override
				protected Map<String, String> getParams() {
					Map<String, String> params = new HashMap<String, String>();
					params.put("email", "nikhil@atlogys.com");
					params.put("password", "123456");

					return params;
				}
			};
			
			mRequestQueue.add(mStringRequest);
		
	}

}
