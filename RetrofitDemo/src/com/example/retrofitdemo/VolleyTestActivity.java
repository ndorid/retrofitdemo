package com.example.retrofitdemo;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

public class VolleyTestActivity extends Activity{
	
	private TextView tv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
		tv = (TextView)findViewById(R.id.tv1);
		
		final ProgressDialog pDialog = new ProgressDialog(VolleyTestActivity.this);
	      pDialog.setMessage("Loading...");
	      pDialog.show();
		
		RequestQueue queue = Volley.newRequestQueue(this); 
	    final String url = "http://www.csitebooks.org/api/get_recent_posts/";

	    // prepare the Request
	    JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
	        new Response.Listener<JSONObject>() 
	        {
	            @Override
	            public void onResponse(JSONObject response) {   
	                            // display response     
	                Log.d("Response", response.toString());
	                tv.setText(response.toString());
	                pDialog.hide();
	                
	                Intent i = new Intent(getApplicationContext(), VolleyPostActivity.class);
					startActivity(i);
	                
	            }
	        }, 
	        new Response.ErrorListener() 
	        {
	             @Override
	             public void onErrorResponse(VolleyError error) {            
	                Log.d("Error.Response", "test");
	                pDialog.hide();
	           }
	        }
	    );

	    // add it to the RequestQueue   
	    queue.add(getRequest);
	}

}
