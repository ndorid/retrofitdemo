package com.example.retrofitdemo;

import retrofit.Callback;
import retrofit.RetrofitError;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.example.retrofitdemo.model.User;


public class MainActivity extends Activity {
	
//	public final String URL = "https://qa.trelliscience.com";

    private TextView tv;
private Button button;
protected String token;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        button = (Button)findViewById(R.id.button1);
        tv = (TextView)findViewById(R.id.tv1);
        
       final String Email = "rajat@atlogys.com";
	final String pass = "rajat@atlogys.com1";
	
	
    RetrofitInterface api = ServiceGenerator.createService(RetrofitInterface.class, RetrofitInterface.BASE_URL);
    
    api.userLogin(Email	, pass, new Callback<User>() {

		@Override
		public void failure(RetrofitError retrofitError) {
			// TODO Auto-generated method stub
			 Log.d("Test", "failure");
		     retrofitError.printStackTrace();
		}

		@Override
		public void success(User apiresponse, retrofit.client.Response response) {
			// TODO Auto-generated method stub
//			Log.d("Test", "success");
			
			try {
				tv.setText("Status: "+apiresponse.getApiResponse().getStatus());
				token = apiresponse.getApiResponse().getToken().getValue();
				Log.d("Test", apiresponse.getApiResponse().getToken().getValue());
				
				Log.d("Date", DateUtils.getRelativeTimeSpanString(apiresponse.getApiResponse().getData().getLastLoginTimestamp(), System.currentTimeMillis()/1000, 0).toString());	
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
		}
    
	
    });
    
    button.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			Intent i = new Intent(getApplicationContext(), GetDocumentActivity.class);
			i.putExtra("token", token);
			startActivity(i);
		}
	});
}
}