package com.example.retrofitdemo;

import java.io.File;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.retrofitdemo.model.User;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

public class GetDocumentActivity extends Activity{

	private TextView tv;
//	private String token;
	Context context = this;
	 OkHttpClient client ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
//		Intent i = getIntent();
//		 token = i.getStringExtra("token");
		
		tv = (TextView)findViewById(R.id.tv1);
		
		client = new OkHttpClient();
		final ConnectionDetector cn = new ConnectionDetector(getApplicationContext());
		
		  RestAdapter.Builder builder = new RestAdapter.Builder()
          .setEndpoint(RetrofitInterface.BASE_URL1)
          .setLogLevel(RestAdapter.LogLevel.FULL)
          .setClient(new OkClient(createCacheForOkHTTP()))
          .setRequestInterceptor(new RequestInterceptor() {
          @Override
          public void intercept(RequestFacade request) {
//              request.addHeader("Accept", "application/json");
//              request.addHeader("token", token);
              if (cn.isConnectingToInternet()) {
                  int maxAge = 60; // read from cache for 1 minute
                  request.addHeader("Cache-Control", "public, max-age=" + maxAge);
              } else {
                  int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
                  request.addHeader("Cache-Control", 
                      "public, only-if-cached, max-stale=" + maxStale);
              }
          }
      });
		
			  RestAdapter adapter = builder.build();
			  
		RetrofitInterface api = adapter.create(RetrofitInterface.class);
		
		api.test(new Callback<User>() {
			
			@Override
			public void success(User arg0, Response arg1) {
				// TODO Auto-generated method stub
//				if(arg0!=null)
				tv.setText(arg0.toString());
				
				Intent i = new Intent(getApplicationContext(), VolleyTestActivity.class);
				startActivity(i);
				
			}
			
			@Override
			public void failure(RetrofitError arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	
	}
		 
		    
		    public  OkHttpClient createCacheForOkHTTP() {
		        Cache cache = null;
		        try {
		        	cache = new Cache(getDirectory(), 1024 * 1024 * 10);
		        } catch (Exception e) {
		            Log.e("OKHttp", "Could not create http cache", e);
		        }
		        if (cache != null) {
		            client.setCache(cache);
		        }

				return client;
		    }

		    //returns the file to store cached details
		    private  File getDirectory() {
		    	String path = "thisiscache";
		    return new File(context.getCacheDir(), path);
		    }
}
